### Question: Why can't I check out?

   ** Check for the following common issues:

      * Do you have read/write access on the repository? In larger organizations/projects it is common to lock out commit/checkout permissions to 'official collabators'.  

      * Are you using https or ssh to checkout?  
         > `git checkout https://username@example.com/foo.git'  

         This type of operation is often used for non authenticated operations. In this context, non-authenticated means you have no requirement to use a ssh key or other 2 Factor Authentication method to act on the repository.

	 'git checkout git@example.com/foo.git' however, will require use of either a project supplied/recognized __ssh key__ / __deployment key__  **OR** a registered one setup in that CVS's account settings.
