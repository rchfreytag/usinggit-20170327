### Question: What is a Branch?

Branches are best described as Sub-Project/Feature standalone segments of code.  One __branches__ code when they wish to add their own bits of code or in the case of a wiki page often a discussion topic while not affecting the __master branch__ (where all the normal stuff is going on).
